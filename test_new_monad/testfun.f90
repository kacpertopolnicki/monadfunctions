module testfun
   use vecFunTools
   implicit none
   !integer :: numMPI
   !integer :: numOPENMP
   type(domain_type) :: iop
   type(domain_type) :: iiop
   type(domain_type) :: igamma
   type(domain_type) :: xP
   type(domain_type) :: yP
   type(domain_type) :: zP
   type(product_type) :: fun1D
   type(product_type) :: fun2D
   type(product_type) :: fun3D
contains
   subroutine initializetestfun(path , nMPI , nOPENMP)
      character * (*) :: path
      integer :: nMPI , nOPENMP
      numMPI = nMPI
      numOPENMP = nOPENMP
      iop = new_domain(trim(path) , trim("iop"))
      iiop = new_domain(trim(path) , trim("iiop"))
      igamma = new_domain(trim(path) , trim("igamma"))
      xP = new_domain(trim(path) , trim("xP"))
      yP = new_domain(trim(path) , trim("yP"))
      zP = new_domain(trim(path) , trim("zP"))
      fun1D = new_product(trim(path) , trim("fun1D"))
      fun2D = new_product(trim(path) , trim("fun2D"))
      fun3D = new_product(trim(path) , trim("fun3D"))
   end subroutine initializetestfun
   subroutine finalizetestfun()
      call clear_domain(iop)
      call clear_domain(iiop)
      call clear_domain(igamma)
      call clear_domain(xP)
      call clear_domain(yP)
      call clear_domain(zP)
      call clear_product(fun1D)
      call clear_product(fun2D)
      call clear_product(fun3D)
   end subroutine finalizetestfun
end module testfun 
