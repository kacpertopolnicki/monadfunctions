program test
	use testfun
	implicit none

	double precision , allocatable :: f1D(:)
	integer , dimension(1) :: f1Dindexes

	double precision , allocatable :: f2D(:)
	integer , dimension(4) :: f2Dindexes
	
	double precision , allocatable :: f3D(:)
	integer , dimension(5) :: f3Dindexes
	
	integer(8) :: ii

	double precision , dimension(128 , 2) :: xPointsWeights
	double precision , dimension(64 , 2) :: yPointsWeights
	double precision , dimension(32 , 2) :: zPointsWeights

	integer(8) :: jj
	double precision :: intervalue
	double precision :: xval
	double precision :: yval
	double precision :: zval

	type(prod_inter_type) :: inter1D
	type(prod_inter_type) :: inter2D
	type(prod_inter_type) :: inter3D

	double precision :: total , total1
	integer :: ix , iy , iz
	integer :: igam
	integer , dimension(5) :: fromind
	integer :: jjold
	integer , dimension(5) :: fromind1

	! load points

	open(12 , file = "xPointsWeights" , status = "old")
		do ii = 1 , 128
			f1Dindexes = from_index(fun1D , ii)
			read(12 , *) xPointsWeights(ii , 1) , xPointsWeights(ii , 2) 
		end do
	close(12)

	open(12 , file = "yPointsWeights" , status = "old")
		do ii = 1 , 64
			f2Dindexes = from_index(fun2D , ii)
			read(12 , *) yPointsWeights(ii , 1) , yPointsWeights(ii , 2) 
		end do
	close(12)

	open(12 , file = "zPointsWeights" , status = "old")
		do ii = 1 , 32
			f3Dindexes = from_index(fun3D , ii)
			read(12 , *) zPointsWeights(ii , 1) , zPointsWeights(ii , 2) 
		end do
	close(12)

	! initialize scalar functions

	call initializetestfun("./testfun/" , 1 , 1)

	print * , 'test 1D function product size (schould be 0.0) : ' , fun1D%product_size - 128
	print * , 'test 1D function chunk size (schould be 0.0) : ' , fun1D%chunk_size - 1
	print * , 'test 2D function product size (schould be 0.0) : ' , fun2D%product_size - 131072  
	print * , 'test 2D function chunk size (schould be 0.0) : ' , fun2D%chunk_size - 16
	print * , 'test 3D function product size (schould be 0.0) : ' , fun3D%product_size - 134217728 
	print * , 'test 3D function chunk size (schould be 0.0) : ' , fun3D%chunk_size - 512

	! allocate functions

	allocate(f1D(fun1D%product_size))
	allocate(f2D(fun2D%product_size))
	allocate(f3D(fun3D%product_size))

	! set function values

	do ii = 1 , fun1D%product_size
		f1Dindexes = from_index(fun1D , ii)
		f1D(ii) = test1D(xPointsWeights(f1Dindexes(1) , 1))
	end do

	do ii = 1 , fun2D%product_size
		f2Dindexes = from_index(fun2D , ii)
		f2D(ii) = test2D(xPointsWeights(f2Dindexes(2) , 1) , yPointsWeights(f2Dindexes(3) , 1))
	end do

	do ii = 1 , fun3D%product_size
		f3Dindexes = from_index(fun3D , ii)
		f3D(ii) = test3D(xPointsWeights(f3Dindexes(2) , 1) , yPointsWeights(f3Dindexes(4) , 1) , zPointsWeights(f3Dindexes(5) , 1))
	end do

	! test to index and from index

	total = 0.0D0
	total1 = 0.0D0
	jjold = 0

	do ix = 1 , 128
		do iy = 1 , 64
			do iz = 1 , 32
				do igam = 1 , 8
					do ii = 1 , 64
						jj = to_index(fun3D , (/int(ii) , ix , igam , iy , iz/))
						total1 = total1 + abs(dble(jj) - dble(jjold) - 1.0D0)
						!if((ix .eq. 1) .and. (iy .eq. 1) .and. (iz .le. 2)) then
						!		print * , jj
						!end if
						fromind = from_index(fun3D , jj) 
						total = total + abs(dble(fromind(1) - ii)) 
						total = total + abs(dble(fromind(2) - ix))
						total = total + abs(dble(fromind(3) - igam))
						total = total + abs(dble(fromind(4) - iy)) 
						total = total + abs(dble(fromind(5) - iz))
						jjold = jj
					end do
				end do
			end do
		end do
	end do

	jjold = 0

	do ix = 1 , 128
		do iy = 1 , 64
			do igam = 1 , 8
				do ii = 1 , 2
					jj = to_index(fun2D , (/int(ii) , ix , iy , igam/))
					total1 = total1 + abs(dble(jj) - dble(jjold) - 1.0D0)
					!if((ix .eq. 1) .and. (iy .eq. 1) .and. (iz .le. 2)) then
					!		print * , jj
					!end if
					fromind1 = from_index(fun2D , jj) 
					total = total + abs(dble(fromind1(1) - ii)) 
					total = total + abs(dble(fromind1(2) - ix))
					total = total + abs(dble(fromind1(3) - iy)) 
					total = total + abs(dble(fromind1(4) - igam))
					jjold = jj
				end do
			end do
		end do
	end do

	print * , 'testing to_index and from_index (should be 0.0): ' , total
	print * , 'testing chuk-ation (should be 0.0): ' , total1

	total = 0.0D0
	do ii = 1 , 131072 
		fromind1 = from_index(fun2D , ii)
		total = total + abs(dble(ii) - dble(to_index(fun2D , fromind1)))
	end do 

	print * , 'testing from_index and to_index (schould be 0.0) : ' , total

	! test 1D interpolation

	inter1D = new_product_interpolate(fun1D , (/-1/))

	open(12 , file = "inter1Dtest" , status = "replace")
		do ii = 0 , 1000
			xval = -4.0D0 + (dble(ii) * 8.0D0 / 1000.0D0)
			call update_product_interpolate(inter1D , (/xval/))
			intervalue = 0.0D0
			do jj = 1 , inter1D%inter_points
				intervalue = intervalue + inter1D%weights(jj) * f1D(inter1D%indexes(jj)) 
			end do
			write(12 , *) xval , test1D(xval) , intervalue
		end do
	close(12)

!	open(12 , file = "inter1Dtest_points" , status = "replace")
!		do ii = 1 , 128
!			xval = xPointsWeights(ii , 1)
!			write(12 , *) xval , test1D(xval)
!		end do
!	close(12)

	! test 2D interpolation
	
	inter2D = new_product_interpolate(fun2D , (/2 , -1 , -1 , 3/))

	open(12 , file = "inter2Dtest_1" , status = "replace")
		do ii = 0 , 1000
			xval = -4.0D0 + (dble(ii) * 8.0D0 / 1000.0D0)
			yval = 1.0D0
			call update_product_interpolate(inter2D , (/xval  , yval/))
			intervalue = 0.0D0
			do jj = 1 , inter2D%inter_points
				intervalue = intervalue + inter2D%weights(jj) * f2D(inter2D%indexes(jj)) 
			end do
			write(12 , *) xval , yval , test2D(xval , yval) , intervalue
		end do
	close(12)

!	open(12 , file = "inter2Dtest_points_1" , status = "replace")
!			do ii = 1 , 128
!			xval = xPointsWeights(ii , 1)
!			yval = 1.0D0
!			write(12 , *) xval , yval , test2D(xval , yval)
!		end do
!	close(12)

	open(12 , file = "inter2Dtest_2" , status = "replace")
		do ii = 0 , 1000
			xval = -1.0D0
			yval = -2.0D0 + (dble(ii) * 9.0D0 / 1000)
			call update_product_interpolate(inter2D , (/xval  , yval/))
			intervalue = 0.0D0
			do jj = 1 , inter2D%inter_points
				intervalue = intervalue + inter2D%weights(jj) * f2D(inter2D%indexes(jj)) 
			end do
			write(12 , *) xval , yval , test2D(xval , yval) , intervalue
		end do
	close(12)

!	open(12 , file = "inter2Dtest_points_2" , status = "replace")
!			do ii = 1 , 64
!			xval = -1.0D0
!			yval = yPointsWeights(ii , 1)
!			write(12 , *) xval , yval , test2D(xval , yval)
!		end do
!	close(12)

	! test 3D interpolations

	inter3D = new_product_interpolate(fun3D , (/3 , -1 , 2 , -1 , -1/))
	
	open(12 , file = "inter3Dtest_1" , status = "replace")
		do ii = 0 , 1000
			xval = -4.0D0 + (dble(ii) * 8.0D0 / 1000.0D0)
			yval = 1.0D0
			zval = 3.0D0
			call update_product_interpolate(inter3D , (/xval  , yval , zval/))
			intervalue = 0.0D0
			do jj = 1 , inter3D%inter_points
				intervalue = intervalue + inter3D%weights(jj) * f3D(inter3D%indexes(jj)) 
			end do
			write(12 , *) xval , yval , zval , test3D(xval , yval , zval) , intervalue
		end do
	close(12)

	open(12 , file = "inter3Dtest_2" , status = "replace")
		do ii = 0 , 1000
			xval = -3.0D0
			yval = -2.0D0 + (dble(ii) * 9.0D0 / 1000)
			zval = 3.0D0
			call update_product_interpolate(inter3D , (/xval  , yval , zval/))
			intervalue = 0.0D0
			do jj = 1 , inter3D%inter_points
				intervalue = intervalue + inter3D%weights(jj) * f3D(inter3D%indexes(jj)) 
			end do
			write(12 , *) xval , yval , zval , test3D(xval , yval , zval) , intervalue
		end do
	close(12)

	open(12 , file = "inter3Dtest_3" , status = "replace")
		do ii = 0 , 1000
			xval = -3.0D0
			yval = 0.1D0
			zval = -3 + (dble(ii) * 8.0D0 / 1000)
			call update_product_interpolate(inter3D , (/xval  , yval , zval/))
			intervalue = 0.0D0
			do jj = 1 , inter3D%inter_points
				intervalue = intervalue + inter3D%weights(jj) * f3D(inter3D%indexes(jj)) 
			end do
			write(12 , *) xval , yval , zval , test3D(xval , yval , zval) , intervalue
		end do
	close(12)

	print * , 'test interpolations : see make_function_plots.nb for plots'

	! test 1D integrations

	total = 0.0D0

	do ix = 1 , 128

		total = total + f1D(to_index(fun1D , (/ix/))) * get_integ_weight(xp , ix)

	end do

	print * , 'test 1D integration (integral approx -0.132093, test value schould be approx 0.0): ' , &
		abs(total - (-0.132093D0))

	! test 2D integrations

	total = 0.0D0

	do ix = 1 , 128
		do iy = 1 , 64

			total = total + f2D(to_index(fun2D , (/3 , ix ,iy , 2/))) * &
				get_integ_weight(xp , ix) * &
				get_integ_weight(yp , iy)

		end do
	end do

	print * , 'test 2D integration (integral approx -0.54036, test value schould be approx 0.0): ' , &
		abs(total - (-0.54036D0))

	! test 3D integrations

	total = 0.0d0

	do ix = 1 , 128
		do iy = 1 , 64
			do iz = 1 , 32

				total = total + f3D(to_index(fun3D , (/3 , ix , 2 , iy , iz/))) * &
					get_integ_weight(xp , ix) * &
					get_integ_weight(yp , iy) * &
					get_integ_weight(zp , iz) 

			end do
		end do
	end do

	print * , 'test 3D integration (integral approx 1.38439, test value schould be approx 0.0): ' , &
		abs(total - (1.38439D0))

	! test 2D integration with changed limits

	total = 0.0D0

	do ix = 1 , 128
		do iy = 1 , 64

			xval = get_integ_point(xp , ix , -1.0D0 , 1.0D0)
			yval = get_integ_point(yp , iy , -0.5D0 , 3.0D0)
			call update_product_interpolate(inter2D , (/xval  , yval/))
			intervalue = 0.0D0
			do jj = 1 , inter2D%inter_points
				intervalue = intervalue + inter2D%weights(jj) * f2D(inter2D%indexes(jj)) 
			end do
			total = total + intervalue * &
				get_integ_weight(xp , ix , -1.0D0 , 1.0D0) * &
				get_integ_weight(yp , iy , -0.5D0 , 3.0D0)

		end do
	end do

	print * , 'test changed limits (integral approx 0.188535, test value schould be approx 0.0): ' , &
		abs(total - (0.188535))

	! test changing integratino limits

	! clear interpolation 

	call clear_product_interpolate(inter1D)
	call clear_product_interpolate(inter2D)
	call clear_product_interpolate(inter3D)

	! deallocate functions

	deallocate(f3D)
	deallocate(f2D)
	deallocate(f1D)

	! clear test fun

	call finalizetestfun()

	contains

	double precision function test1D(x)
		double precision :: x
		test1D = (1.0D0 / (x * x + 1.0D0)) * cos(5.0D0 * x) * sin(7.0D0 * (x - 1.0D0))
		return
	end function test1D

	double precision function test2D(x , y)
		double precision :: x , y
		test2D = -((x*(-2 + y)*sin(1 - x)*sin(2*x*y))/(1 + x**2 + y**2)) 
		return
	end function test2D

	double precision function test3D(x , y , z)
		double precision :: x , y , z
		test3D = -((x*(-2 + y)*exp(z)*sin(1 - x)*sin(2*x*y)*sin(z))/(1 + x**2 + y**2))
		return
	end function test3D
end program test
