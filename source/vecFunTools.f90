module vecFunTools
	implicit none

	! Type for representing a domain of floating point numbers
	!
	! kind
	!    1 - finite set of discrete values
	!    2 - domain of floating point numbers
	! nof_points
	!    the number of points and weights
	! points
	!    List of points.
	!    Functions on the domain will be 
	!    represented as arrays of values for these points.
	! weights
	!    list of integration weights that correspond to -points-
	! nof_neighbourhood
	!    number of points in -neighbourhood-
	! l_neighborhood
	!    If left neighbour can be determined for a range then 
  !    contains left neighbour. Contains 0 otherwise.
	! r_neighborhood
	!    If right neighbour cab be detemined for a range then
	!    contains right neighbour. Contains 0 otherwise.
	! c_neighborhood
	!    If point in range then contains point number
	!    containst 0 otherwise.
	! neighborhood_step
	!    Obly for -kind .eq. 2- (domain of floating point numbers).
	!    The element of -(lrc)_neighbourhood(ii)- contains indexes to 
	!    points in relation to the range:
	!       (points(1) + neighbourhood_step * (dble(ii) - 1) ,
	!       points(1) + neighbourhood_step * (dble(ii)))
	! a , b
	!    Limits of the domain.
	type domain_type
		! For all kinds:
		integer :: kind
		integer :: nof_points
		double precision , allocatable :: points(:)
		double precision , allocatable :: weights(:)
		!! For floating point number domains only:
		!!double precision , allocatable :: qarray(: , :)
		integer :: nof_neighborhood
		double precision :: neighborhood_step  
		integer , allocatable :: l_neighborhood(:) 
		integer , allocatable :: r_neighborhood(:)
		integer , allocatable :: c_neighborhood(:)
		double precision :: a , b
	end type domain_type

	! Type for products
	!
	! domains
	!    list of product domains
	! intPER
	!    permutations of 1 , 2 , ... for per
	! intREP
	!    permutations of 1 , 2 , ... for rep
	! isDistributed
	!    T - domain is distributed
	!    F - domain in not distributed
	! chunk_size
	!    number of vector coordinates to
	!    handle in a single process
	! product size
	!    total number of vector coordinates
	type product_type
		integer :: nof_domains
		type(domain_type) , allocatable :: domains(:)
		integer , allocatable :: intPER(:)
		integer , allocatable :: intREP(:)
		logical , allocatable :: isDistributed(:)
		integer(8) :: chunk_size
		integer(8) :: product_size
		integer(8) , allocatable :: mul(:)
	end type product_type

	! Type for interpolating over products
	!
	! my_product
	!    product related to this interpolation data
	! inter_points
	!    number of points needed for the interpolation
	!    this is a power of 4
	! inter_dimensions
	!    number of dimensions that will be interpolated
	! indexes
	!    list of indexes in the product that will be used to
	!    calculate the interpolation
	! weights
	!    list of weights that correspond to indexes
	! i_weights
	!    table contains interpolation weights
	!    rach column corresponds to a single interpolated dimension
	!    each row corresponds to one of te four neighbouring points
	! i_neigh
	!    table contains indexes of points needed for the interpolation
	!    each column corresponds to a single interpolated dimension
	!    each row corresponds to one of the four neighbouring points
	! new_index
	!    contains indexes in the product, -1 corresponds to an interpolated
	!    dimensions
	type prod_inter_type
		type(product_type) :: my_product
		integer :: inter_points
		integer :: inter_dimensions
		integer , allocatable :: indexes(:)
		double precision , allocatable :: weights(:)
		integer , allocatable :: inter_indexes(:)
		double precision , allocatable :: i_weights(: , :)
		integer , allocatable :: i_neigh(: , :)
		integer , allocatable :: new_index(:)
	end type prod_inter_type

	integer :: numMPI
	integer :: numOPENMP

	contains

	! Make a new interpolation type
	!
	! prod
	!    product domain for the interpolation
	! ind
	!    list of indexes from the product domain
	!    the value -1 means that this dimension will be interpolated
	type(prod_inter_type) function new_product_interpolate(prod , ind)
		type(product_type) :: prod
		integer , dimension(prod%nof_domains) :: ind

!		integer , allocatable :: iinndd(:)

		integer :: ii
		integer :: jj

		new_product_interpolate%my_product = prod
		
		allocate(new_product_interpolate%new_index(prod%nof_domains))

		new_product_interpolate%inter_points = 1
		new_product_interpolate%inter_dimensions = 0
		do ii = 1 , prod%nof_domains
			new_product_interpolate%new_index(ii) = ind(ii)
			if(ind(ii) .le. 0) then
				new_product_interpolate%inter_points = new_product_interpolate%inter_points * 4
				new_product_interpolate%inter_dimensions = new_product_interpolate%inter_dimensions + 1
			end if
		end do

!		allocate(iinndd(new_product_interpolate%inter_dimensions))

		allocate(new_product_interpolate%inter_indexes(new_product_interpolate%inter_dimensions))
		allocate(new_product_interpolate%i_weights(new_product_interpolate%inter_dimensions , 4))
		allocate(new_product_interpolate%i_neigh(new_product_interpolate%inter_dimensions , 4))

		allocate(new_product_interpolate%weights(new_product_interpolate%inter_points))
		allocate(new_product_interpolate%indexes(new_product_interpolate%inter_points))

		jj = 1
		do ii = 1 , prod%nof_domains
			if(ind(ii) .le. 0) then
				new_product_interpolate%inter_indexes(jj) = ii
				jj = jj + 1
			end if
		end do

!		do ii = 1 , new_product_interpolate%inter_dimensions
!			new_product_interpolate%i_neigh(ii , :) = get_neighbor_point( &
!				prod%domains(new_product_interpolate%inter_indexes(ii)) , &
!				val(new_product_interpolate%inter_indexes(ii)) , &
!				prod%domains(new_product_interpolate%inter_indexes(ii))%a , &
!				prod%domains(new_product_interpolate%inter_indexes(ii))%b , &
!				)
!			new_product_interpolate%i_weights(ii , :) = get_inter_weights( &
!				prod%domains(new_product_interpolate%inter_indexes(ii)) , &
!				new_product_interpolate%i_neigh(ii , :) , &
!				val(new_product_interpolate%inter_indexes(ii)) , &
!				prod%domains(new_product_interpolate%inter_indexes(ii))%a , &
!				prod%domains(new_product_interpolate%inter_indexes(ii))%b , &
!				)
!		end do
!
!		do ii = 1 , new_product_interpolate%inter_points
!			new_product_interpolate%weights(ii) = 1.0D0
!			do jj = 1 , new_product_interpolate%inter_dimensions
!				iinndd(jj) = mod(ii - 1 , 4**(jj - 1)) + 1
!				new_index(new_product_interpolate%inter_indexes(jj)) = new_product_interpolate%i_neigh(jj , iinndd(jj))
!			end do
!			new_product_interpolate%weights(ii) = to_index(prod , new_index)
!		end do
!		
!		deallocate(iinndd)
	end function new_product_interpolate

	! Update the interpolation type
	!
	! inter
	!    the interpolation type
	! val
	!    table with values for the new interpolation
	subroutine update_product_interpolate(inter , val)
		type(prod_inter_type) :: inter
		double precision , dimension(inter%inter_dimensions) :: val
		
		integer :: ii , jj , mmuull , nei_ind
		double precision :: wweeiigghhtt

		integer , dimension(inter%inter_dimensions) :: iinndd
		integer , dimension(inter%my_product%nof_domains) :: n_ind

		do ii = 1 , inter%my_product%nof_domains
			n_ind(ii) = inter%new_index(ii)
		end do

		do ii = 1 , inter%inter_dimensions
			inter%i_neigh(ii , :) = get_neighbor_point( &
				inter%my_product%domains(inter%inter_indexes(ii)) , &
				val(ii) , &
				inter%my_product%domains(inter%inter_indexes(ii))%a , &
				inter%my_product%domains(inter%inter_indexes(ii))%b &
				)
			inter%i_weights(ii , :) = get_inter_weights( &
				inter%my_product%domains(inter%inter_indexes(ii)) , &
				inter%i_neigh(ii , :) , &
				val(ii) , &
				inter%my_product%domains(inter%inter_indexes(ii))%a , &
				inter%my_product%domains(inter%inter_indexes(ii))%b &
				)
		end do

		do ii = 1 , inter%inter_points
			mmuull = 1
			wweeiigghhtt = 1.0D0
			do jj = 1 , inter%inter_dimensions
				iinndd(jj) = mod((ii - 1) / mmuull , 4) + 1
				n_ind(inter%inter_indexes(jj)) = inter%i_neigh(jj , iinndd(jj))
				wweeiigghhtt = wweeiigghhtt * inter%i_weights(jj , iinndd(jj))
				mmuull = mmuull * 4
			end do
			nei_ind = to_index(inter%my_product , n_ind)
			inter%indexes(ii) = nei_ind
			inter%weights(ii) = wweeiigghhtt
		end do

!		do ii = 1 , inter%inter_points
!			inter%weights(ii) = 1.0D0
!			do jj = 1 , inter%inter_dimensions
!				iinndd(jj) = mod(ii - 1 , 4**(jj - 1)) + 1
!				n_ind(inter%inter_indexes(jj)) = inter%i_neigh(jj , iinndd(jj))
!			end do
!			inter%weights(ii) = to_index(inter%my_product , inter%new_index)
!		end do

	end subroutine update_product_interpolate
		
	! Clear interpolation product type
	!
	! inter
	!    the interpolation type
	subroutine clear_product_interpolate(inter)
		type(prod_inter_type) :: inter
		deallocate(inter%indexes)
		deallocate(inter%weights)
		deallocate(inter%inter_indexes)
		deallocate(inter%i_weights)
		deallocate(inter%i_neigh)
		deallocate(inter%new_index)
	end subroutine clear_product_interpolate

	! Create a new product domain.
	! 
	! base_dir
	!    directory with the points, weights and product domain files
	! file_fun
	!    file with the domains for the product, the syntax is:
	!    #FUN
	!    <number of domains>
	!    <domain 1>
	!    <domain 2>
	!    ...
	!    <
	!     list of permutations for per
	!     eg:
	!     per(1)
	!     per(2)
	!     ...
	!    >
	!    <
	!     list of permutations for rep
	!     eg:
	!     rep(1)
	!     rep(2)
	!     ...
	!    >
	!    <
	!     list of characters designating that domain
	!     is distributed (T) or not distributed (F)
	!     eg:
	!     T
	!     F
	!     ...
	!    >
	type(product_type) function new_product(base_dir , file_fun)
		character * (*) :: base_dir
		character * (*) :: file_fun
		character * 4 :: kind_str
		character * (100) :: domain_file
		integer :: ii , jj
		integer :: mm
		open(12 , file = trim(base_dir)//trim(file_fun) , status = "old")
			read(12 , *) kind_str
			if(trim(kind_str) .ne. "#FUN") then
				write(0 , *) "[ERROR] Expecting #FUN at the beginning of product file. Exiting."
				call exit(1)
			end if
			read(12 , *) new_product%nof_domains
			allocate(new_product%domains(new_product%nof_domains))
			do ii = 1 , new_product%nof_domains
				read(12 , *) domain_file
				new_product%domains(ii) = new_domain(base_dir , domain_file)
			end do
			allocate(new_product%intPER(new_product%nof_domains))
			do ii = 1 , new_product%nof_domains
				read(12 , *) new_product%intPER(ii)
			end do
			allocate(new_product%intREP(new_product%nof_domains))
			do ii = 1 , new_product%nof_domains
				read(12 , *) new_product%intREP(ii)
			end do
			allocate(new_product%mul(new_product%nof_domains))
			mm = 1
			do ii = new_product%nof_domains , 1 , -1
				jj = new_product%intREP(ii)
				new_product%mul(jj) = mm
				mm = mm * new_product%domains(jj)%nof_points
			end do
			allocate(new_product%isDistributed(new_product%nof_domains))
			new_product%chunk_size = 1
			do ii = 1 , new_product%nof_domains
				read(12 , *) new_product%isDistributed(ii)
				if(.not. new_product%isDistributed(ii)) then
					new_product%chunk_size = new_product%chunk_size * new_product%domains(ii)%nof_points
				end if
			end do
			new_product%product_size = 1
			do ii = 1 , new_product%nof_domains
				new_product%product_size = new_product%product_size * new_product%domains(ii)%nof_points
			end do
		close(12)
	end function new_product

	! Returns the index to a value of the product.
	!
	! fun
	!    the product domain with a list of integer or floating point domains
	! indexes
	!    list of indexes
	integer(8) function to_index(fun , indexes)
		type(product_type) :: fun
		integer , dimension(fun%nof_domains) :: indexes
		integer :: ii
		to_index = 1
		do ii = 1 , fun%nof_domains
			to_index = to_index + fun%mul(ii) * (indexes(ii) - 1)	
		end do
		return
	end function to_index

	function from_index(fun , ind)
		type(product_type) :: fun
		integer(8) :: ind
		integer , dimension(fun%nof_domains) :: from_index
		integer :: ii
		do ii = 1 , fun%nof_domains
			from_index(ii) = mod(((ind - 1) / fun%mul(ii)) , fun%domains(ii)%nof_points)  +1
		end do
		return 
	end function from_index

	! Clear up data in a product domain.
	!
	! fun
	!    the product domain with a list of integer or floating point domains
	subroutine clear_product(fun)
		type(product_type) :: fun
		integer :: ii
		do ii = 1 , fun%nof_domains
			call clear_domain(fun%domains(ii))
		end do
		deallocate(fun%domains)
		deallocate(fun%intPER)
		deallocate(fun%intREP)
		deallocate(fun%isDistributed)
		deallocate(fun%mul)
	end subroutine clear_product

	! Initializes a new instance of a type representing a floating point 
	! number domain.
	!
	! base_dir
	!    path to directory with the points and weights
	! file_pts
	!    name of file with points and weights
	!    syntax:
	!    #FLO | #INT
	!    <number of points>
	!    <point> <weight>
	!    <point> <weight>
	!    ...
	type (domain_type) function new_domain(base_dir , file_pts)
		character * (*) :: base_dir
		character * (*) :: file_pts
		integer :: ii , jj
		double precision :: step
		double precision :: length
		character * 4 :: kind_str
		double precision :: st , en
		open(11 , file = trim(base_dir)//trim(file_pts) , status = "old")
			read(11 , *) kind_str 
			read(11 , *) new_domain%nof_points
			read(11 , *) new_domain%a , new_domain%b
			if(trim(kind_str) .eq. "#INT") then
				new_domain%kind = 1
			else if(trim(kind_str) .eq. "#FLO") then
				new_domain%kind = 2
			else
				write(0 , *) "[ERROR] Expecting #INT or #FLO at beginning of domain point file. Exiting."
				call exit(1)
			end if
			if(new_domain%nof_points .le. 2 .and. new_domain%kind .eq. 2) then
				write(0 , *) "[ERROR] The number of points for floating point domain has to be greater then 2. Exiting."
				call exit(1)
			end if
			allocate(new_domain%points(new_domain%nof_points))
			allocate(new_domain%weights(new_domain%nof_points))
			do ii = 1 , new_domain%nof_points
				read(11 , *) new_domain%points(ii) 
			end do
			if(new_domain%kind .eq. 2) then
				do ii = 1 , new_domain%nof_points
					read(11 , *) new_domain%weights(ii) 
				end do
			else
				do ii = 1 , new_domain%nof_points
					new_domain%weights(ii) = 1.0
				end do
			end if
		close(11)
		if(new_domain%kind .eq. 2) then
			length = new_domain%points(new_domain%nof_points) - new_domain%points(1)
			step = length
			do ii = 1 , new_domain%nof_points - 1
				if(new_domain%points(ii + 1) - new_domain%points(ii) < step) then
					step = new_domain%points(ii + 1) - new_domain%points(ii)
				end if
			end do
			step = step / 3.0D0
			new_domain%neighborhood_step = step
			new_domain%nof_neighborhood = int((length / step) + 0.5D0) + 6
			allocate(new_domain%l_neighborhood(new_domain%nof_neighborhood))
			allocate(new_domain%c_neighborhood(new_domain%nof_neighborhood))
			allocate(new_domain%r_neighborhood(new_domain%nof_neighborhood))
			new_domain%l_neighborhood = 0.0D0
			new_domain%c_neighborhood = 0.0D0
			new_domain%r_neighborhood = 0.0D0
			do ii = 1 , new_domain%nof_neighborhood
				st = new_domain%points(1) - 0.5D0 * step + (dble(ii) - 1.0D0) * step
				en = new_domain%points(1) - 0.5D0 * step + (dble(ii)) * step
				do jj = 1 , new_domain%nof_points
					if((new_domain%points(jj) .lt. en) .and. (new_domain%points(jj) .ge. st)) then
						new_domain%c_neighborhood(ii) = jj
					end if
				end do
			end do
			jj = 1
			do ii = 1 , new_domain%nof_neighborhood
				if(new_domain%c_neighborhood(ii) .ne. 0) then
					jj = new_domain%c_neighborhood(ii)
				else
					new_domain%l_neighborhood(ii) = jj
				end if
			end do
			jj = new_domain%nof_points
			do ii = new_domain%nof_neighborhood , 1 , -1
				if(new_domain%c_neighborhood(ii) .ne. 0) then
					jj = new_domain%c_neighborhood(ii)
				else
					new_domain%r_neighborhood(ii) = jj
				end if
			end do
		end if
	end function new_domain

	! Get point for integration.
	!
	! domain
	!    the domain with the points
	! jj
	!    number of point
	! aaa
	!    optional, left limit of the integration
	! bbb
	!    optional , right limit of the integration
	double precision function get_integ_point(domain , jj , aaa , bbb)
		type(domain_type) :: domain
		integer :: jj
		double precision , optional :: aaa , bbb

		double precision :: aa , bb
		
		if(.not. present(aaa)) then
			aa = domain%a
		else
			aa = aaa
		end if
		if(.not. present(bbb)) then
			bb = domain%b
		else
			bb = bbb
		end if
		
		if(domain%kind .eq. 2) then

			get_integ_point = aa + (bb - aa) * (domain%points(jj) - domain%a) / (domain%b - domain%a)
			return

		elseif(domain%kind .eq. 1) then
			
			get_integ_point = domain%points(jj)
			return

		end if
	end function get_integ_point

	! Get weight for integration.
	!
	! domain
	!    the domain with the points
	! jj
	!    number of point
	! aaa
	!    optional , left limit of the integration
	! bbb
	!    optional , right limit of the integration
	double precision function get_integ_weight(domain , jj , aaa , bbb)
		type(domain_type) :: domain
		integer :: jj
		double precision , optional :: aaa , bbb

		double precision :: aa , bb
		
		if(.not. present(aaa)) then
			aa = domain%a
		else
			aa = aaa
		end if
		if(.not. present(bbb)) then
			bb = domain%b
		else
			bb = bbb
		end if
		
		if(domain%kind .eq. 2) then

			!matmul(domain%qarray , ptab)
			get_integ_weight = (bb - aa) * domain%weights(jj) / (domain%b - domain%a)
			return

		elseif(domain%kind .eq. 1) then
			
			get_integ_weight = 1.0D0
			return

		end if
	end function get_integ_weight

	! Clean up the data from the domain.
	!
	! domain
	!    the domain with the points
	subroutine clear_domain(domain)
		type(domain_type) :: domain
		deallocate(domain%points)
		deallocate(domain%weights)
		if(domain%kind .eq. 2) then
			deallocate(domain%l_neighborhood)
			deallocate(domain%c_neighborhood)
			deallocate(domain%r_neighborhood)
		end if
	end subroutine clear_domain

	! Get list of integers with neighbours:
	!    (i1 , i2 , i3 , i4)
	! where:
	!    i2 - nearest neighbour to the left
	!    i3 - nearest neighbour to the right
	!    i1 - second nearest neighbour to the left
	!    i4 - second nearest neighbour to the right
	!
	! domain
	!    the domain with the points
	! x
	!    the point
	! aaa
	!    optional , left limit of the integration
	! bbb
	!    optional , right limit of the integration
	function get_neighbor_point(domain , x , aaa , bbb)
		integer , dimension(4) :: get_neighbor_point
		type (domain_type) :: domain
		double precision :: x , point
		double precision , optional :: aaa , bbb

		double precision :: aa , bb
		
		integer :: ii	
		! closest neighbour to the left:
		integer :: point_index 

		integer :: iil , iir , iic

		if(.not. present(aaa)) then
			aa = domain%a
		else
			aa = aaa
		end if
		if(.not. present(bbb)) then
			bb = domain%b
		else
			bb = bbb
		end if
		
		point = domain%a + (domain%b - domain%a) * ((x - aa) / (bb - aa))

		if(domain%kind .ne. 2) then
			write(0 , *) "[ERROR] get_neighbour_point called with an integer domain. Exiting."
			call exit(1)
		end if

		if(point .lt. domain%points(1)) then
			get_neighbor_point(1) = 1
			get_neighbor_point(2) = 1
			get_neighbor_point(3) = 1
			get_neighbor_point(4) = 2
			return
		else if(point .gt. domain%points(domain%nof_points)) then
			get_neighbor_point(1) = domain%nof_points - 1
			get_neighbor_point(2) = domain%nof_points 
			get_neighbor_point(3) = domain%nof_points
			get_neighbor_point(4) = domain%nof_points
			return
		else
			point_index = int((point - (domain%points(1) - 0.5D0 * domain%neighborhood_step)) / domain%neighborhood_step) + 1
		
			iil = domain%l_neighborhood(point_index)
			iir = domain%r_neighborhood(point_index)
			iic = domain%c_neighborhood(point_index)
			if(iil .ne. 0) then
				if(iir .eq. 0) then
					write(0 , *) "[ERROR] Determined left neighbour but not right. Exiting."
					call exit(1)
				end if
				get_neighbor_point(1) = iil - 1
				get_neighbor_point(2) = iil
				get_neighbor_point(3) = iir
				get_neighbor_point(4) = iir + 1
			else if(iic .ne. 0) then
				if(domain%points(iic) .ge. point) then
					get_neighbor_point(1) = iic - 2
					get_neighbor_point(2) = iic - 1
					get_neighbor_point(3) = iic 
					get_neighbor_point(4) = iic + 1 
				else
					get_neighbor_point(1) = iic - 1
					get_neighbor_point(2) = iic 
					get_neighbor_point(3) = iic + 1
					get_neighbor_point(4) = iic + 2 
				end if
			else
				write(0 , *) "[ERROR] Could not determine point in range. Exiting."
				call exit(1)
			end if

			do ii = 1 , 4
				if(get_neighbor_point(ii) .lt. 1) get_neighbor_point(ii) = 1
				if(get_neighbor_point(ii) .gt. domain%nof_points) get_neighbor_point(ii) = domain%nof_points
			end do
			
			return
		end if
	end function get_neighbor_point

	! Get a list of interpolation weights. 
	!    
	! domain
	!    the domain with the points
	! nei
	!    list with indexes of the 4 neighbours
	! x
	!    the point
	! aaa , bbb
	!    optional , domain limits
	function get_inter_weights(domain , nei , x , aaa , bbb)
		double precision , dimension(4) :: get_inter_weights
		type (domain_type) :: domain
		integer , dimension(4) :: nei
		double precision :: x
		double precision , optional :: aaa , bbb

		double precision :: aa , bb

		double precision :: x1 , x2 , x3 , x4

		double precision :: aL32 , aL21 , aR43 , aR32
		double precision :: d32 , d21 , d43

		if(.not. present(aaa)) then
			aa = domain%a
		else
			aa = aaa
		end if
		if(.not. present(bbb)) then
			bb = domain%b
		else
			bb = bbb
		end if

		if(nei(1) .eq. nei(3)) then
			get_inter_weights(1) = 1.0D0
			get_inter_weights(2) = 0.0D0
			get_inter_weights(3) = 0.0D0
			get_inter_weights(4) = 0.0D0
			return
		end if
		if(nei(2) .eq. nei(4)) then
			get_inter_weights(1) = 0.0D0
			get_inter_weights(2) = 0.0D0
			get_inter_weights(3) = 0.0D0
			get_inter_weights(4) = 1.0D0
			return
		end if

		x1 = aa + (bb - aa) * (domain%points(nei(1)) - domain%a) / (domain%b - domain%a)
		x2 = aa + (bb - aa) * (domain%points(nei(2)) - domain%a) / (domain%b - domain%a)
		x3 = aa + (bb - aa) * (domain%points(nei(3)) - domain%a) / (domain%b - domain%a)
		x4 = aa + (bb - aa) * (domain%points(nei(4)) - domain%a) / (domain%b - domain%a)

		if(nei(2) .ne. nei(1)) then
			d21 = x2 - x1
			aL21 = 0.5D0
			aL32 = 0.5D0
		else
			d21 = 1.0D0
			aL21 = 0.0D0
			aL32 = 1.0D0
		end if

		if(nei(4) .ne. nei(3)) then
			d43 = x4 - x3
			aR43 = 0.5D0
			aR32 = 0.5D0
		else
			d43 = 1.0D0
			aR43 = 0.0D0
			aR32 = 1.0D0
		end if

		d32 = 1.0D0

		get_inter_weights(1) = -((aL21*(x - x2)*(x - x3)**2)/(d21*(x2 - x3)**2))
		get_inter_weights(2) = &
			((x - x3)*(-(d21*(x - x2)*(aR32*(x - x2) + aL32*(x - x3))*(x2 - x3)) + &
			aL21*d32*(x - x2)*(x - x3)*(x2 - x3) - d21*d32*(x - x3)*(2*x - 3*x2 + x3)))/&
			(d21*d32*(x2 - x3)**3)
		get_inter_weights(3) = &
			-(((x - x2)*(d32*(x - x2)*(-(d43*(2*x + x2 - 3*x3)) + aR43*(x - x3)*(x2 - x3)) + &
			d43*(aR32*(x - x2) + aL32*(x - x3))*(x - x3)*(-x2 + x3)))/&
			(d32*d43*(x2 - x3)**3))
		get_inter_weights(4) = (aR43*(x - x2)**2*(x - x3))/(d43*(x2 - x3)**2)
!		get_inter_weights(1) = ((x - X2)*(x - X3)**2)/(2.*(X1 - X2)*(X2 - X3)**2)
!		get_inter_weights(2) = &
!			((x - X3)*(x**2*(-2*X1 + X2 + X3) - X2*(X2**2 - 4*X2*X3 + X3**2) + &
!			X1*(X2**2 - 5*X2*X3 + 2*X3**2) - x*(2*X2**2 + X2*X3 + X3**2 - X1*(3*X2 + X3))))/ &
!			(2.*(X1 - X2)*(X2 - X3)**3)
!		get_inter_weights(3) = &
!			((x - X2)*(-(X3*(X2**2 - 4*X2*X3 + X3**2)) - &
!			x*(X2**2 + X2*X3 + 2*X3**2) + x**2*(X2 + X3 - 2*X4) + x*(X2 + 3*X3)*X4 + &
!			(2*X2**2 - 5*X2*X3 + X3**2)*X4))/(2.*(X2 - X3)**3*(X3 - X4))
!		get_inter_weights(4) = ((x - X2)**2*(x - X3))/(2.*(X2 - X3)**2*(-X3 + X4)) 
!		get_inter_weights(1) = ((x - x2)*(x - x3)**2)/((x1 - x3)*(x2 - x3)**2) 
!		get_inter_weights(2) = -(((x - x3)*(-(x*x2*(x2 + 3*x3)) - x2*(x2**2 - 4*x2*x3 + x3**2) + &
!											x**2*(x2 + x3 - 2*x4) + x3*(-3*x2 + x3)*x4 + x*(3*x2 + x3)*x4))/ &
!										((x2 - x3)**3*(x2 - x4)))
!		get_inter_weights(3) = ((x - x2)*(x1*x2*(x2 - 3*x3) + x**2*(-2*x1 + x2 + x3) - &
!										x*x3*(3*x2 + x3) + x*x1*(x2 + 3*x3) - x3*(x2**2 - 4*x2*x3 + x3**2)))/&
!										((x2 - x3)**3*(-x1 + x3))
!		get_inter_weights(4) = ((x - x2)**2*(x - x3))/((x2 - x3)**2*(-x2 + x4))
		return
	end function get_inter_weights
	integer(8) function startVectorRange(prod , rankMPI , rankOPENMP)
		type(product_type) :: prod
		integer :: rankMPI
		integer :: rankOPENMP
		startVectorRange = (rankMPI * numOPENMP + rankOPENMP) * (prod%product_size / (numMPI * numOPENMP)) + 1
		return
	end function startVectorRange

	integer(8) function endVectorRange(prod , rankMPI , rankOPENMP)
		type(product_type) :: prod
		integer :: rankMPI
		integer :: rankOPENMP
		endVectorRange = (rankMPI * numOPENMP + rankOPENMP + 1) * (prod%product_size / (numMPI * numOPENMP))
		return
	end function endVectorRange

	logical function checkNumThreads(prod)
		type(product_type) :: prod
		
		logical :: ok

		ok = .true.

		ok = ok .and. (mod(prod%product_size , numMPI * numOPENMP) .eq. 0)
		ok = ok .and. (mod(prod%product_size , numMPI) .eq. 0)
		ok = ok .and. (mod(prod%product_size / (numMPI * numOPENMP) , prod%chunk_size) .eq. 0)

		checkNumThreads = ok
		return
	end function checkNumThreads
end module vecFunTools
