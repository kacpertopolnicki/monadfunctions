(* ::Package:: *)

BeginPackage["monad`"];


monad::usage ="Defined by user for m via bind[m] and return[m].
monad[m] represents a monad for m. Creating instances of the monad is via:
   monad[m][x] 
where:
   monad[m][x] :: m a
and:
   x :: a";


return::usage = "Defined by user for m. Together with bind schould satisfy monadic laws.
return[m][x]
where:
   x :: a
evaluates to an object of type:
   m a
";


bind::usage = "Defined by user for m. Together with return schould satisty monadic laws.
bind[m][a , f]
is equivalent to:
   ma >>= f
where:
   ma :: m a
   f :: a -> m b
and evaluates to an object of type:
   m b
";


do::usage = "Implements the do notation for m. For example:
do[m][
   a \[LeftArrow] ... ,
   b \[LeftArrow] ... ,
   return f[a,b]
]
where:
   f[a , b] :: c
evaluates to an object of types:
   m c
";


LeftArrow::usage = "\[LeftArrow] is used in do notation.";


Begin["`Private`"];


hasHead::usage = "Returns expr if the Head of expr is head.";
makeFunction::usage = "Makes a function that checks the head of the resulting expression.";


SetAttributes[makeFunction , HoldAll];
SetAttributes[isMonad , HoldAll];
isMonad[m_][expr_]:=
	If[MatchQ[Head[expr] , monad[m]] , 
		expr , 
		Throw["Expecting function to return monad - 
the head of the result schould match 
   monad["<>ToString[m]<>"]. 
Got:
   "<>ToString[Head[expr]]<>"
instead."]
	];
makeFunction[m_][x_ , expr_] := 
Function[x , isMonad[m][expr]];


bindTMP::usage="Temporary symbol for bind.";
functionTMP::usage = "Temporary symbol for function.";


return[m_][x___]:=Throw["Bad arguments in return, expecting one argument. Got:\n"<>ToString[{x}]];


LeftArrow[]:=Throw["Expecting two arguments in LeftArrow, got none."];
LeftArrow[x_]:=Throw["Expecting two arguments in LeftArrow, got one."];
LeftArrow[x_ , y_ , __]:=Throw["Expecting two arguments in LeftArrow, got more then two."];
fst[LeftArrow[x_ , y_]]:= x;
snd[LeftArrow[x_ , y_]]:= y;
do[m_][x___ , y_ , r:Except[_LeftArrow]]:= 
  If[MatchQ[y , LeftArrow[_ , _]] , 
     do[m][x , bindTMP[m][snd[y] , functionTMP[m][fst[y] , r]]],
     do[m][x , bindTMP[m][y , functionTMP[m][Unique["nonExistantVariable"] , r]]]
  ];
do[m_][r:Except[_LeftArrow]]:=
(r/.functionTMP->makeFunction)/.bindTMP->bind;


End[];


EndPackage[];
