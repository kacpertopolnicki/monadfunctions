(* ::Package:: *)

packageDirectory::usage = "Directory containing this package.";
packageDirectory = DirectoryName[$InputFileName];
AppendTo[$Path , packageDirectory];


BeginPackage["vecFun`" , {"monad`"}];


gs::usage = "Generalized sum monad.
monad[gs][x , state , files , code]
x - any mathematica expression
state - Association with state
files - Association with files
code - list of strings with code
code = {variables , init , fin , sub}
variables - list of module variable declarations
init - list of code to initialize the module
fin - list of code to finalize the module
sub - list of subroutines and functions
";
elementFrom::usage = "elementFrom[domain] 
indicates element from domain.";
floatDomain::usage = "floatDomain[{-0.5 , 0.0 , 0.5} , {1.0 , 1.0 , 1.0} , {-1. , 1.}] 
Domain of floating point functions from 
   -1.0 
to 
   1.0. 
Any functions having arguments in this 
domain will be discretized on the grid:
   {-0.5 , 0.0 , 1.0}
The second argument:
   {1.0 , 1.0 , 1.0}
contains weights for integrating over the domain.";
intDomain::usage = "intDomain[{-1/2 , 0 , 1/2} , {2 , 3}]
Domain of functions with discrete values. Functions having
arguments in this domain can take values from:
   {-1/2 , 0 , 1/2}[[2;;3]]";
addFloat::usage = "addFloat[{-0.5 , 0.0 , 0.5} , {0.5 , 1.0 , 0.5} , {-1. , 1.}] 
Adds the domain of floating point functions from 
   -1.0 
to 
   1.0. 
to the state. Any functions having arguments in this 
domain will be discretized on the grid:
   {-0.5 , 0.0 , 1.0}
Integrations over this domain will be performed using the weights:
   {0.5 , 1.0 , 0.5}
The integration interval, integration points and integration weights
can be changed using functions from:
   vecFunTools

Options:
   symbolName
the name of the symbol for the domain.
";
addInt::usage = "addInt[{-1/2 , 0 , 1/2} , {2 , 3}]
Adds the domain of functions with discrete values
to the state. Functions having arguments in this 
domain can take values from:
   {-1/2 , 0 , 1/2}[[2;;3]]

Options:
   symbolName
the name of the symbol for the domain.
";
symbolName::usage = "String that will be used as a base for unique symbol names.";
addProductDomain::usage = "addProductDomain[{{elementFrom[domain12] , True},{elementFrom[domain13] , False},{elementFrom[int123] , True}}]
Represents the domain of functions of three arguments.
The three arguments are from domains:
   domain12, domain13, int11
respecively. The value
   True
by each domain marks arguments from this domain for distribution over MPI and OPENMP processes.
The value:
   False
by each domain means that all function arguments from this domain will be in each MPI and OPENMP process.

Options:
   symbolName
the name for the product domain.";
productExpr::usage = "productExpression[{{elementFrom[domain12] , True},{elementFrom[domain13] , False},{elementFrom[int123] , True}}]
Part of the state. Similar to addProductDomain.";
productFrom::usage = "productFrom[domain]
Indicates an element from:
   domain";


(*makeTable::usage = "Option for addSum. 
If set to True, a tabularized version of the generalized sum will be created.
The default value is False.";*)
(*addSum::usage = "addSum[expr , {i , p}]
Creates a generalized sum of expr, over the domains i , p.";*)
(*sumExpr::usage = "sumExpr[expr , {i , p} , tab]
Represents the generalized sum of expr over the domains i , p.
If tab is True then a table for the values of the sum will be created.";*)


(*linearOperator::usage = "linearOperator[]";
operatorExpression::usage = "operatorExpression[]";*)


makeModule::usage = "makeModule[dir , name][mod]
Created the module:
   name
and appropriate files in:
   dir
for operator";
updateDomains::usage = "updateDomains[dir][mod]
Upodate files for the domains in:
   dir"


Begin["`Private`"];


symbols = {};


uUnique[s_String]:=
Module[
   {
      sym = Symbol[s],
      res
   },
   If[Not[ValueQ[Evaluate[sym]]] && Not[ContainsAny[symbols , {s}]] , 
      res = sym;
      , 
      res = Unique[s];
   ];
   AppendTo[symbols , ToString[res]];
   res
   ];


return[gs][x_]:= monad[gs][x , Association[] , Association[] , {{} , {} , {} , {}}];
bind[gs][a_ , f_]:= With[
	{
		fa = f[a[[1]]]
	},
	monad[gs][fa[[1]] , 
		Join[a[[2]] , fa[[2]]], (*state*)
		Join[a[[3]] , fa[[3]]], (*files*)
		{
			Join[a[[4]][[1]] , fa[[4]][[1]]] , (*module variable declarations*) 
			Join[a[[4]][[2]] , fa[[4]][[2]]] , (*code for subroutine that initializes the module*)
			Join[a[[4]][[3]] , fa[[4]][[3]]] , (*code for subroutine that finalizes the module*)
			Join[a[[4]][[4]] , fa[[4]][[4]]]   (*additional subroutines and functions (after contains in module)*)
		} 
		]
];


makeStateElement[floatDomain[pts_ , wts_ , {a_ , b_}]]:=
Module[
	{
		result = ""
	},
	result = result<>"#FLO\n"<>ToString[Dimensions[pts][[1]]]<>"\n"<>ToString[FortranForm[a]]<>" "<>ToString[FortranForm[b]]<>"\n";
	Do[
		result = result<>ToString[FortranForm[pts[[ii]]]]<>"\n";
	, {ii , 1 , Dimensions[pts][[1]]}];
	Do[
		result = result<>ToString[FortranForm[wts[[ii]]]]<>"\n";
	, {ii , 1 , Dimensions[wts][[1]]}];
	result
];


makeStateElement[intDomain[pts_ , {a_ , b_}]]:=
Module[
	{
		result = ""
	},
	result = result<>"#INT\n"<>ToString[Dimensions[pts][[1]]]<>"\n"<>ToString[FortranForm[a]]<>" "<>ToString[FortranForm[b]]<>"\n";
	Do[
		result = result<>ToString[FortranForm[pts[[ii]]]]<>"\n";
	, {ii , 1 , Dimensions[pts][[1]]}];
	result
];


makeStateElement[productExpr[domains_List]]:=
Module[
	{
		unsortedDomains = domains[[;; , 1]],
		sortedDomains = Sort[domains , #1[[2]]&][[;; , 1]],
		per , rep , result
	},
	rep = Permute[Range[Dimensions[domains][[1]]] , FindPermutation[unsortedDomains , sortedDomains]];
	per = Permute[Range[Dimensions[domains][[1]]] , InversePermutation[FindPermutation[unsortedDomains , sortedDomains]]];
	(*Print[unsortedDomains];
	Print[per];
	Print[rep];
	Print[sortedDomains];
	Print["--"];*)
	result = "#FUN\n"<>ToString[Dimensions[domains][[1]]]<>"\n";
	result = result <> StringJoin[(ToString[#[[1 , 1]]]<>"\n")&/@domains];
	result = result <> ((ToString[#]<>"\n")&/@per);
	result = result <> ((ToString[#]<>"\n")&/@rep);
	result = result <> StringJoin[(If[#[[2]] , "T" , "F"]<>"\n")&/@domains];
	result
];


updateDomains[where_ , dirName_][monad[gs][op_ , state_ , files_ , code_]]:=
Module[{keys},
keys = keys = Keys[files];
If[FileNames[dirName] == {},
	CreateDirectory[dirName]
];
Do[ 
		WriteString[File[FileNameJoin[{where , dirName , ToString[keys[[ii]]]}]] , files[keys[[ii]]]];
	, {ii , 1 , Dimensions[keys][[1]]}];
	where
];
makeModule[where_, moduleName_][monad[gs][op_ , state_ , files_ , code_]]:=
Module[
	{
		startModule = 
"module "<>moduleName<>"
   use vecFunTools
   implicit none
   !integer :: numMPI
   !integer :: numOPENMP
",
		endModule = 
"end module "<>moduleName<>" 
",
		startInitSubroutine = 
"   subroutine initialize"<>moduleName<>"(path , nMPI , nOPENMP)
      character * (*) :: path
      integer :: nMPI , nOPENMP
      numMPI = nMPI
      numOPENMP = nOPENMP
",
		endInitSubroutine = 
"   end subroutine initialize"<>moduleName<>"
",
		startFinalizeSubroutine = 
"   subroutine finalize"<>moduleName<>"()
",
		endFinalizeSubroutine = 
"   end subroutine finalize"<>moduleName<>"
",
		module = "",
		keys
	},
	module = module<>startModule;
	module = module<>StringJoin[code[[1]]];
	module = module<>"contains\n";
	module = module<>startInitSubroutine;
	module = module<>StringJoin[code[[2]]];
	module = module<>endInitSubroutine;
	module = module<>startFinalizeSubroutine;
	module = module<>StringJoin[code[[3]]];
	module = module<>endFinalizeSubroutine;
	module = module<>endModule;
	(*keys = Keys[files];
	Do[ 
		WriteString[File[FileNameJoin[{where , ToString[keys[[ii]]]}]] , files[keys[[ii]]]];
	, {ii , 1 , Dimensions[keys][[1]]}];*)
	WriteString[File[FileNameJoin[{where , ToString[moduleName]<>".f90"}]] , module];
	where
];


Options[addFloat] = {symbolName -> "domain"};
addFloat[x___]:=Throw["Bad arguments in addFloat.\n"<>ToString[{x}]];
addFloat[pts_List , wts_List , {a_/;NumberQ[a],b_/;NumberQ[b]} , OptionsPattern[]]:= 
With[
	{
		unique = uUnique[OptionValue[symbolName]],
		domain = floatDomain[pts , wts , {a , b}]
	},
	If[Dimensions[pts] == Dimensions[wts],
		monad[gs][
			elementFrom[unique] , 
			Association[unique -> domain] , 
			Association[unique -> makeStateElement[domain]],
			{
				{"   type(domain_type) :: "<>ToString[unique]<>"\n"} , 
				{"      "<>ToString[unique]<>" = new_domain(trim(path) , trim(\""<>ToString[unique]<>"\"))\n"} ,
				{"      call clear_domain("<>ToString[unique]<>")\n"} ,
				{}
			}
		]
		,
		Throw["The dimensions of points does not match the dimensions of weights in addFloat."];
	]
];
Options[addInt] = {symbolName -> "domain"};
addInt[x___]:=Throw["Bad arguments in addInt.\n"<>ToString[x]];
addInt[pts_List , {a_/;NumberQ[a],b_/;NumberQ[b]} , OptionsPattern[]]:= 
With[
	{
		unique = uUnique[OptionValue[symbolName]],
		domain = intDomain[pts , {a , b}]
	},
	monad[gs][
		elementFrom[unique] , 
		Association[unique -> domain],
		Association[unique -> makeStateElement[domain]],
		{
			{"   type(domain_type) :: "<>ToString[unique]<>"\n"} , 
			{"      "<>ToString[unique]<>" = new_domain(trim(path) , trim(\""<>ToString[unique]<>"\"))\n"} ,
			{"      call clear_domain("<>ToString[unique]<>")\n"} ,
			{}
		}	
	]
];


Options[addProductDomain] = {symbolName -> "domain"};
addProductDomain[domains:{{elementFrom[_] , True | False}..} , OptionsPattern[]]:= 
Module[
	{
		unique = uUnique[OptionValue[symbolName]],
		domain = productExpr[domains]
	},
	monad[gs][
		productFrom[unique , #[[1,1]]&/@domains] , 
		Association[unique -> domain],
		Association[unique -> makeStateElement[domain]],
		{
			{"   type(product_type) :: "<>ToString[unique]<>"\n"} , 
			{"      "<>ToString[unique]<>" = new_product(trim(path) , trim(\""<>ToString[unique]<>"\"))\n"} ,
			{"      call clear_product("<>ToString[unique]<>")\n"} ,
			{}
		}	
	]
];


(*isSymbol::usage = "isSymbol[expr] 
returns -True- if there is no value associated to -expr-, -expr- is not a number and 
-expr- is an atom.";
isSymbol[symbol_]:=AtomQ[symbol]&&Not[ValueQ[symbol]]&&Not[NumberQ[symbol]];
SetAttributes[isSymbol , HoldAll];
Options[addOperator] = {symbolName -> "operator"};*)


(*CAREFULL: functions in bind are not applied to domains!*)
(*The result is a lack of substitution in the final result!*)
(*This is the way it schould work!?!?*)
(*Options[addSum]={makeTable->False};
addSum[expr_ , domains_List , OptionsPattern[]]:= With[
	{
		unique = uUnique["gensum"]
	},
	monad[gs][
		sumExpr[expr , domains , OptionValue[makeTable]] , 
		Association[],
		Association[],
		{{} , {} , {} , {}}
	]
];*)


(*multiply by "x":*)
(*polyShift[poly_List]:=Join[{0 },poly[[1;;-2]]];*)
(*multiply by number:*)
(*polyMul[x_ , poly_List]:=x poly;*)
(*add polynomials:*)
(*polyAdd[poly1_List , poly2_List]:=poly1 + poly2;*)
(*calculates coefficients of polynomial from roots:*)
(*polyCf[roots_List]:= Module[{fin , mulnum , mulx},
fin = Table[
	If[ii == 1 , 1 , 0] , {ii , 1 , Dimensions[roots][[1]]+1}];
	Do[
		mulnum = polyMul[-roots[[ii]] , fin];
		mulx = polyShift[fin];
		fin= polyAdd[mulnum , mulx]; 
		, {ii , 1 , Dimensions[roots][[1]]}
	];
	fin
];*)
(*polyInt[roots_List , a_ , b_]:=Module[{coef},
	coef = polyCf[roots];
	Sum[
		coef[[ii]] 1/(ii - 1 + 1) (b^(ii - 1 + 1) - a^(ii - 1 + 1)) 
	, {ii , 1 , Dimensions[coef][[1]]}]
];*)
(*polyIntTab[roots_List , a_ , b_]:=Module[{coef},
	coef = polyCf[roots];
	Table[
		coef[[ii]] 1/(ii - 1 + 1) 
	, {ii , 1 , Dimensions[coef][[1]]}]
];*)
(*newCoef[roots_List , a_ , b_]:=Module[{c , i},
	c = Table[
		Product[
			If[ii!= jj , roots[[jj]] - roots[[ii]] , 1.0]
				, {ii , 1 , Dimensions[roots][[1]]}] 
				, {jj , 1 , Dimensions[roots][[1]]}
			];
	i = Table[
		polyInt[Delete[roots , jj] , a , b] 
		, {jj , 1 , Dimensions[roots][[1]]}];
i/c
];*)
(*newCoefTab[roots_List , a_ , b_]:=Module[{c , i},
	c = Table[
		Product[
			If[ii!= jj , roots[[jj]] - roots[[ii]] , 1.0]
				, {ii , 1 , Dimensions[roots][[1]]}] 
				, {jj , 1 , Dimensions[roots][[1]]}
			];
	i = Table[
		polyIntTab[Delete[roots , jj] , a , b] / c[[jj]]
		, {jj , 1 , Dimensions[roots][[1]]}];
i
];*)


(*linearOperator[expr_ , arguments_List , sums_List]:= monad[gs][
	operatorExpression[expr , arguments , sums] , 
	Association[],
	Association[],
	{{} , {} , {} , {}}
];*)


End[];


EndPackage[];
