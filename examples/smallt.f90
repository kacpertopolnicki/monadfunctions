module smallt
   use vecFunTools
   implicit none
   !integer :: numMPI
   !integer :: numOPENMP
   type(domain_type) :: iop
   type(domain_type) :: iiso
   type(domain_type) :: eD
   type(domain_type) :: pD
   type(domain_type) :: ppD
   type(domain_type) :: xD
   type(domain_type) :: phiD
   type(product_type) :: funt
   type(product_type) :: funppp
contains
   subroutine initializesmallt(path , nMPI , nOPENMP)
      character * (*) :: path
      integer :: nMPI , nOPENMP
      numMPI = nMPI
      numOPENMP = nOPENMP
      iop = new_domain(trim(path) , trim("iop"))
      iiso = new_domain(trim(path) , trim("iiso"))
      eD = new_domain(trim(path) , trim("eD"))
      pD = new_domain(trim(path) , trim("pD"))
      ppD = new_domain(trim(path) , trim("ppD"))
      xD = new_domain(trim(path) , trim("xD"))
      phiD = new_domain(trim(path) , trim("phiD"))
      funt = new_product(trim(path) , trim("funt"))
      funppp = new_product(trim(path) , trim("funppp"))
   end subroutine initializesmallt
   subroutine finalizesmallt()
      call clear_domain(iop)
      call clear_domain(iiso)
      call clear_domain(eD)
      call clear_domain(pD)
      call clear_domain(ppD)
      call clear_domain(xD)
      call clear_domain(phiD)
      call clear_product(funt)
      call clear_product(funppp)
   end subroutine finalizesmallt
end module smallt 
